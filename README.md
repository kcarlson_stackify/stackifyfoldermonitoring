Simple console application that monitors when text files in a specified directory are changed (created, modified, or deleted). Logging to Stackify with NLog is integrated. Simply enter your Stackify API key & optionally Environment & AppName if you'd like it to be different.

To run: 

1. Publish the application.
2. Navigate to the \bin\debug\ folder to find the .exe.
3. Make note of the location or move it somewhere else.
4. Open a Command Prompt window & run: 
```
#!c#

StackifyFolderMonitoring.exe "path\to\watched\folder\"
```

The app will watch the changes until you hit 'q' in the prompt.